# RSSReaderV2



# RSS Reader Application

## Overview

This Python command-line application, named `rss_reader`, is designed to fetch and display RSS feed data. 
It provides functionalities to retrieve and present news topics from a specified RSS URL. 
The application supports various options, such as limiting the number of news topics, filtering by date, 
and displaying results in different formats.

## Prerequisites

- Python 3.x
- Required Python packages can be installed using the following command:
  ```bash
  pip install -r requirements.txt
  
## Usage:
  ```code
   python main.py [--source SOURCE] [--json] [--limit LIMIT] [--date DATE] [--verbose]

Command-line Arguments
--source: RSS URL to fetch news topics.
--json: Print result as JSON in stdout.
--limit: Limit the number of news topics.
--date: Date for which news is required (format: YYYYMMDD).
--verbose: Display data in a wide format.
```

## Exception Handling
The application includes custom exception handling to provide informative error messages. If there is no data to show or if an error occurs during execution, relevant exceptions will be raised.

## Dependencies
```
rss_reader: Module containing the main functionality for parsing RSS feeds.
requests: Library for making HTTP requests.
util: Module containing utility functions for caching, filtering, and printing data.
custom_exception: Module defining a custom exception (NoDataException) for cases when no data is available.
```