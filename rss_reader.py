import xml.etree.ElementTree as et
import pandas as pd
from datetime import date
from util import date_parser
from custom_exception import ParserException


def parse_item(item: et.Element, root_source: str) -> pd.Series:
    """
    Parse an RSS item and format it as a dictionary.

    Args:
        item: ElementTree.Element representing an RSS item.
        root_source : root source link
    Returns:
        Series: Formatted RSS item data as a Series object
    """
    output_format = '%Y%m%d'
    item = pd.Series(dict({
        "source": root_source,
        "pubDate": pd.to_datetime(date_parser(item.find("pubDate").text)) if (item.find("pubDate") is not None)
        else date.today().strftime(output_format),
        "title": item.find("title").text if (item.find("title") is not None) else "",
        "link": item.find("link").text if (item.find("link") is not None) else "",
        "source_name": item.find("source").text if (item.find("source") is not None) else "",
        "guid": item.find("guid").text if (item.find("guid") is not None) else "",
    }))
    return item


def rss_parser(xml: str, source: str) -> pd.DataFrame:
    """
    RSS parser.

    Args:
        xml: XML document as a string.
        source: source link from args to add for every item.

    Returns:
        Pandas DataFrame.
    """
    result = []

    try:
        root = et.fromstring(xml)
        channel = root.find(".//channel")
        if channel is not None:
            items = channel.findall(".//item")
            for item in items:
                item_data = parse_item(item, source)
                result.append(item_data)

    except Exception as e:
        raise ParserException(e)

    return pd.DataFrame(result)
