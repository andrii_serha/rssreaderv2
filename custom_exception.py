class NoDataException(Exception):
    pass


class ParserException(Exception):
    pass
