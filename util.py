import json as js
import pandas as pd
import os
from dateutil.parser import parse
from datetime import date


def save_to_cache(df: pd.DataFrame):
    try:
        if not os.path.exists('cached_data.pkl'):
            df.to_pickle('cached_data.pkl')
        else:
            cached = pd.DataFrame(pd.read_pickle('cached_data.pkl'))
            new_df = pd.concat([cached, df], ignore_index=True).drop_duplicates(ignore_index=True)
            new_df.to_pickle('cached_data.pkl')
    except Exception as e:
        raise Exception(f"Failed to read/write content to/from pickle file: {e}")


def read_from_cache() -> pd.DataFrame:
    try:
        if not os.path.exists('cached_data.pkl'):
            raise Exception("No data in cache, please specify RSS source!")
        else:
            return pd.DataFrame(pd.read_pickle('cached_data.pkl'))
    except Exception as e:
        raise Exception(f"Failed to read/write content to/from pickle file: {e}")


def filtering_data(not_filtered_df: pd.DataFrame, source, arg_date: str) -> pd.DataFrame:
    date_arg = pd.to_datetime(date_parser(arg_date))
    if source:
        return not_filtered_df[(not_filtered_df['source'] == str(source)) & (not_filtered_df['pubDate'] == date_arg)]
    else:
        return not_filtered_df[not_filtered_df["pubDate"] == date_arg]


def date_parser(input_date: str):
    output_format = '%Y%m%d'
    try:
        return parse(input_date).strftime(output_format)
    except ValueError as e:
        return date.today().strftime(output_format)


def print_as_json(limited_data, verbose: bool):
    result = []
    data_to_print = limited_data if verbose else limited_data[['link', 'pubDate', 'title']]
    for i in range(0, len(data_to_print)):
        element_dict = data_to_print.iloc[i].to_dict()
        element_dict["pubDate"] = str(element_dict["pubDate"])

        result.append(js.dumps(element_dict, indent=1, allow_nan=False))

    print("\n".join(result))


def print_as_list(limited_data, verbose: bool):
    for i in range(0, len(limited_data)):
        element_dict = limited_data.iloc[i].to_dict()
        if verbose:
            print(f"source: {element_dict['source']} \n"
                  f"link: {element_dict['link']} \n"
                  f"date: {element_dict['pubDate']} \n"
                  f"title: {element_dict['title']}\n"
                  f"source_name: {element_dict['source_name']}\n"
                  f"guid: {element_dict['guid']}\n")
        else:
            print(f"link: {element_dict['link']} \n"
                  f"date: {element_dict['pubDate']} \n"
                  f"title: {element_dict['title']}\n")
