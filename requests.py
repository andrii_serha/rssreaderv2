import urllib.request


def get(url):
    try:
        response = urllib.request.urlopen(url)
        content = response.read().decode('utf-8')
        return content
    except Exception as e:
        raise Exception(f"Failed to fetch content from {url}: {e}")
