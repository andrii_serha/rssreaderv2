import sys
import rss_reader as rr
from argparse import ArgumentParser
import requests
import util
from datetime import date
from custom_exception import NoDataException

if __name__ == '__main__':
    """
       The main function of your task.
       """
    parser = ArgumentParser(
        prog="rss_reader",
        description="Pandas Python command-line RSS reader.",
    )
    parser.add_argument("main", help="local location of main.py", type=str, nargs="?")
    parser.add_argument("--source", help="RSS URL", type=str, nargs="?", default=None)
    parser.add_argument(
        "--json", help="Print result as JSON in stdout", action="store_true", default=False
    )
    parser.add_argument(
        "--limit", help="Limit news topics if this parameter provided", type=int, default=None
    )
    parser.add_argument(
        "--date", help="Date for which news are required", type=int, default=None
    )
    parser.add_argument(
        "--verbose", help="Wide format of data", action="store_true", default=False
    )
    result = []
    args = parser.parse_args(sys.argv)

    try:
        if args.source and not args.date:
            xml = requests.get(args.source) if args.source else ""
            df = rr.rss_parser(xml, args.source)
            util.save_to_cache(df)
        elif not args.date:
            args.date = date.today().strftime('%Y%m%d')

        if args.date:
            cached_data = util.read_from_cache()
            filtered_data = util.filtering_data(cached_data, args.source, arg_date=str(args.date))

            if filtered_data.empty:
                raise NoDataException("No data to show, please specify source!")

            limited_data = filtered_data[:args.limit if args.limit else len(filtered_data)]

            if args.json:
                util.print_as_json(limited_data, args.verbose)
            else:
                util.print_as_list(limited_data, args.verbose)

    except NoDataException as custom_e:
        print(custom_e)

    except Exception as e:
        raise e
